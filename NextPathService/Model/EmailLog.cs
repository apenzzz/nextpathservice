﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService.Model
{
    public class EmailLog : TableEntity
    {
        public EmailLog()
        {

        }

        public EmailLog(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public long EmailLogId { get; set; }
        public DateTime LogDate { get; set; }
        public string FromEmail { get; set; }
        public string Subdomain { get; set; }
        public string ProjectName { get; set; }
        public string WorkItemId { get; set; }
    }
}