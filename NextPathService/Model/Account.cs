﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService.Model
{
    public class Account : TableEntity
    {
        public Account()
        {

        }

        public Account(string partitionKey, string rowKey)
        {
            this.PartitionKey = partitionKey;
            this.RowKey = rowKey;
        }

        public Guid AccountId { get; set; }
        public string PAT { get; set; }
        public string ProjectName { get; set; }
        public string SimpleProjectName { get; set; } // no whitespace, all lowercase (in email address)
        public string SubdomainName { get; set; }
        public string Email { get; set; }
    }
}