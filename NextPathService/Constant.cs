﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService
{
    public class Constant
    {
        public const string APPSETTING_OFFICE365_USERNAME = "office365_username";
        public const string APPSETTING_OFFICE365_PASSWORD = "office365_password";
        public const string APPSETTING_OFFICE365_DOMAIN = "office365_domain";
        public const string APPSETTING_TOTALEMAIL_PERROUTINE = "totalemail_perroutine";
        public const string APPSETTING_STORAGE_CONNECTIONSTRING = "StorageConnectionString";

        public const string ACCOUNT_TABLENAME = "accounts";
        public const string EMAILLOG_TABLENAME = "emaillogs";
        public const string TASKLOG_TABLENAME = "tasklogs";
    }
}