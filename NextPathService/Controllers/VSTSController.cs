﻿using Microsoft.IdentityModel.Tokens;
using Microsoft.WindowsAzure.Storage.Table;
using NextPathService.Helper;
using NextPathService.Model;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;


namespace NextPathService.Controllers
{
    public class VSTSPostDTO
    {
        public string pat { get; set; }
        public string collectionname { get; set; }
        public string projectname { get; set; }
        public string token { get; set; }
        public string email { get; set; }
    }

    public class LogDTO
    {
        public string collectionname { get; set; }
        public string token { get; set; }
    }

    public class VSTSController : ApiController
    {
        [ActionName("submit")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Post(VSTSPostDTO dto)
        {
            try
            {
                // validate token
                if (!VSTSHelper.ValidateRequest(dto.token))
                    throw new Exception("Token is invalid");

                // check for existing, if not, then insert
                if(AccountHelper.CheckIfAccountExists(dto.projectname, dto.collectionname, dto.email))
                {
                    AccountHelper.UpdateAccount(dto.pat, dto.projectname, dto.collectionname, dto.email);
                }
                else
                {
                    AccountHelper.CreateAccount(dto.pat, dto.projectname, dto.collectionname, dto.email);
                }

                // compose result
                var result = new
                {
                    message = "Ok"
                };

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }

        [ActionName("test")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Get()
        {
            var result = new
            {
                message = "Test OK"
            };

            return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
        }

        [ActionName("topuser")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage TopUser(LogDTO logDTO)
        {
            try
            {
                // validate token
                if (!VSTSHelper.ValidateRequest(logDTO.token))
                    throw new Exception("Token is invalid");

                AzureTableHelper azureTableHelper = new AzureTableHelper();
                CloudTable table = azureTableHelper.GetEmailLogTable();

                var query = new TableQuery<EmailLog>()
                    .Where(TableQuery.GenerateFilterCondition("Subdomain", QueryComparisons.Equal, logDTO.collectionname.ToLower()));
                var list = table.ExecuteQuery(query).ToList();

                var result =
                    from el in list
                    group el by el.FromEmail into g
                    select new
                    {
                        g.Key,
                        EmailCount = g.Count()
                    };

                result = result.OrderByDescending(q => q.EmailCount).Take(5);

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }

        [ActionName("monthly")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Monthly(LogDTO logDTO)
        {
            try
            {
                // validate token
                if (!VSTSHelper.ValidateRequest(logDTO.token))
                    throw new Exception("Token is invalid");

                AzureTableHelper azureTableHelper = new AzureTableHelper();
                CloudTable table = azureTableHelper.GetEmailLogTable();

                var query = new TableQuery<EmailLog>()
                    .Where(TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("Subdomain", QueryComparisons.Equal, logDTO.collectionname.ToLower()),
                    TableOperators.And, TableQuery.GenerateFilterConditionForDate("LogDate", QueryComparisons.GreaterThan, DateTime.Now.AddDays(-30).Date)));
                var list = table.ExecuteQuery(query).ToList();

                var result =
                    from el in list
                    group el by el.LogDate.Date into g
                    select new
                    {
                        Key = g.Key.ToString("M/d"),
                        EmailCount = g.Count()
                    };

                result = result.OrderByDescending(q => q.Key).Take(30);

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }

        [ActionName("yearly")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Yearly(LogDTO logDTO)
        {
            try
            {
                // validate token
                if (!VSTSHelper.ValidateRequest(logDTO.token))
                    throw new Exception("Token is invalid");

                AzureTableHelper azureTableHelper = new AzureTableHelper();
                CloudTable table = azureTableHelper.GetEmailLogTable();

                var query = new TableQuery<EmailLog>()
                    .Where(TableQuery.CombineFilters(TableQuery.GenerateFilterCondition("Subdomain", QueryComparisons.Equal, logDTO.collectionname.ToLower()),
                    TableOperators.And, TableQuery.GenerateFilterConditionForDate("LogDate", QueryComparisons.GreaterThan, DateTime.Now.AddMonths(-11).Date)));
                var list = table.ExecuteQuery(query).ToList();

                var result =
                    from el in list
                    group el by new { el.LogDate.Date.Month, el.LogDate.Date.Year } into g
                    select new
                    {
                        g.Key.Month,
                        Year = g.Key.Year.ToString().Substring(2, 2),
                        EmailCount = g.Count()
                    };

                result = result.OrderByDescending(q => q.Month).OrderByDescending(q => q.Year).Take(12);

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }

        [ActionName("log")]
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage Log(LogDTO logDTO)
        {
            try
            {
                // validate token
                if (!VSTSHelper.ValidateRequest(logDTO.token))
                    throw new Exception("Token is invalid");

                AzureTableHelper azureTableHelper = new AzureTableHelper();
                CloudTable table = azureTableHelper.GetEmailLogTable();

                var query = new TableQuery<EmailLog>()
                    .Where(TableQuery.GenerateFilterCondition("Subdomain", QueryComparisons.Equal, logDTO.collectionname.ToLower()));
                var list = table.ExecuteQuery(query).ToList();

                var result =
                    from l in list
                    select new
                    {
                        l.LogDate,
                        l.FromEmail,
                        l.Subdomain,
                        l.ProjectName,
                        l.WorkItemId
                    };

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }

        [ActionName("forceupdate")]
        [HttpGet]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public HttpResponseMessage ForceUpdate()
        {
            try
            {
                EmailJob emailJob = new EmailJob();
                emailJob.Execute(null);

                var result = new
                {
                    message = "Ok"
                };

                return Request.CreateResponse(HttpStatusCode.OK, result, new JsonMediaTypeFormatter());
            }
            catch (Exception ex)
            {
                var result = new
                {
                    message = ex.Message
                };

                return Request.CreateResponse(HttpStatusCode.BadRequest, result, new JsonMediaTypeFormatter());
            }
        }
    }
}