﻿using Microsoft.Exchange.WebServices.Data;
using NextPathService.Helper;
using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace NextPathService
{
    public class EmailJob : IJob
    {
        static bool RedirectionCallback(string url)
        {
            return url.ToLower().StartsWith("https://");
        }

        public System.Threading.Tasks.Task Execute(IJobExecutionContext context)
        {
            try
            {
                // configuration
                string username = ConfigurationManager.AppSettings[Constant.APPSETTING_OFFICE365_USERNAME];
                string password = ConfigurationManager.AppSettings[Constant.APPSETTING_OFFICE365_PASSWORD];
                string domain = ConfigurationManager.AppSettings[Constant.APPSETTING_OFFICE365_DOMAIN];
                int totalEmailPerRoutine = 10;
                int.TryParse(ConfigurationManager.AppSettings[Constant.APPSETTING_TOTALEMAIL_PERROUTINE], out totalEmailPerRoutine);

                // connect to ews
                ExchangeService service = new ExchangeService();
                service.Credentials = new WebCredentials(username, password);
                service.AutodiscoverUrl(username, RedirectionCallback);

                // check inbox
                Folder inboxFolder = Folder.Bind(service, WellKnownFolderName.Inbox);
                ItemView itemView = new ItemView(totalEmailPerRoutine);
                itemView.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Ascending);
                foreach (EmailMessage email in inboxFolder.FindItems(itemView)) // loop thru new emails
                {
                    HandleEmail(email, domain);
                }

                return System.Threading.Tasks.Task.FromResult<object>(null);
            }
            catch (Exception ex)
            {
                // Email Log
                LogHelper.EmailLog("EmailJob Exception", ex.Message, "", "");

                return System.Threading.Tasks.Task.FromResult<object>(null);
            }
        }

        public void HandleEmail(EmailMessage email, string domain)
        {
            // load property (to load attachment, to, cc)
            PropertySet propertySet = new PropertySet(BasePropertySet.FirstClassProperties);
            email.Load(propertySet);

            // email props
            string fromEmail = email.From.Address;
            string emailBody = EmailReplyParser.Reply(email.Body.Text);

            // get recipient code, asumming any email with _ and our domain is the recipient code, and will take first
            string recipientCode = string.Empty;
            EmailAddressCollection ccRecipients = email.CcRecipients;
            EmailAddressCollection toRecipients = email.ToRecipients;
            var toQuery = toRecipients.Where(q => q.Address.Contains("_")
                && q.Address.Contains(domain)).FirstOrDefault();
            if (toQuery == null)
            {
                var ccQuery = ccRecipients.Where(q => q.Address.Contains("_")
                    && q.Address.Contains(domain)).FirstOrDefault();

                if (ccQuery == null)
                {
                    // spam then, no recipient code
                    email.Delete(DeleteMode.MoveToDeletedItems);
                    return;
                }
                else
                {
                    recipientCode = ccQuery.Address;
                }
            }
            else
            {
                recipientCode = toQuery.Address;
            }

            // Parse email address (foo_123 logic)
            string vstsSubDomain = recipientCode.Split('@')[0].Split('_')[0];
            string vstsSimpleProjectName = recipientCode.Split('@')[0].Split('_')[1];
            string vstsWorkItemId = recipientCode.Split('@')[0].Split('_')[2];

            // Fetch PAT for this VSTS Domain
            string pat = AccountHelper.GetPAT(vstsSubDomain, vstsSimpleProjectName, fromEmail);
            if (pat == string.Empty)
            {
                email.Delete(DeleteMode.MoveToDeletedItems);
                return;
            }

            string fullProjectName = AccountHelper.GetProjectName(vstsSubDomain, vstsSimpleProjectName);

            // handle attachment
            if (email.Attachments.Count > 0)
            {
                foreach (var attachment in email.Attachments)
                {
                    if(attachment is FileAttachment)
                    {
                        FileAttachment fileAttachment = attachment as FileAttachment;
                        fileAttachment.Load();

                        MemoryStream ms = new MemoryStream(fileAttachment.Content);

                        var postAttachmentTask = VSTSHelper.PostAttachment(vstsSubDomain, fullProjectName, pat, ms, attachment.Name);
                        postAttachmentTask.Wait();

                        string attachmentLink = postAttachmentTask.Result;

                        if (fileAttachment.IsInline)
                        {
                            emailBody = emailBody.Replace("cid:" + fileAttachment.ContentId, attachmentLink);
                        }
                        else
                        {
                            emailBody += "<a href='" + attachmentLink + "'>" + attachment.Name + "</a><br/>";
                        }
                    }
                }
            }

            // Post work item comment
            var postTask = VSTSHelper.PostComment(vstsSubDomain, fullProjectName, pat, vstsWorkItemId, emailBody);
            postTask.Wait();
            bool isPostTaskSuccess = postTask.Result;

            if (isPostTaskSuccess == true)
            {
                // Email Log
                LogHelper.EmailLog(fromEmail, fullProjectName, vstsSubDomain, vstsWorkItemId);

                // Delete processed email message
                email.Delete(DeleteMode.MoveToDeletedItems);
            }
        }
    }
}