﻿using NextPathService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService.Helper
{
    public class LogHelper
    {
        public static void EmailLog(string fromEmail, string projectName, string subdomain, string workItemId)
        {
            EmailLog newEmailLog = new EmailLog(subdomain, Guid.NewGuid().ToString())
            {
                FromEmail = fromEmail,
                LogDate = DateTime.Now,
                ProjectName = projectName,
                Subdomain = subdomain,
                WorkItemId = workItemId
            };

            AzureTableHelper azureTableHelper = new AzureTableHelper();
            azureTableHelper.InsertEmailLog(newEmailLog);
        }
    }
}