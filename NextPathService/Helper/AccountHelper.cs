﻿using Microsoft.WindowsAzure.Storage.Table;
using NextPathService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService.Helper
{
    public class AccountHelper
    {
        public static void CreateAccount(string pat, string projectName, string collectionName, string email)
        {
            Account newAccount = new Account(collectionName, Guid.NewGuid().ToString())
            {
                AccountId = Guid.NewGuid(),
                PAT = pat,
                ProjectName = projectName.ToLower(),
                SimpleProjectName = projectName.Trim().ToLower().Replace(" ", string.Empty),
                SubdomainName = collectionName.ToLower(),
                Email = email.ToLower()
            };

            AzureTableHelper azureTableHelper = new AzureTableHelper();
            azureTableHelper.UpsertAccount(newAccount);
        }

        public static void UpdateAccount(string pat, string projectName, string collectionName, string email)
        {
            AzureTableHelper azureTableHelper = new AzureTableHelper();
            CloudTable table = azureTableHelper.GetAccountTable();

            TableQuery<Account> query = new TableQuery<Account>()
                .Where(TableQuery.GenerateFilterCondition("SubdomainName", QueryComparisons.Equal, collectionName.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("ProjectName", QueryComparisons.Equal, projectName.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("Email", QueryComparisons.Equal, email.ToLower()));

            var result = table.ExecuteQuery(query).ToList();
            if (result.Count > 0)
                azureTableHelper.UpsertAccount(result[0]);
        }

        public static string GetPAT(string subdomain, string simpleProjectName, string email)
        {
            string pat = string.Empty;

            AzureTableHelper azureTableHelper = new AzureTableHelper();
            CloudTable table = azureTableHelper.GetAccountTable();

            TableQuery<Account> query = new TableQuery<Account>().Where(TableQuery.GenerateFilterCondition("SubdomainName", QueryComparisons.Equal, subdomain.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("SimpleProjectName", QueryComparisons.Equal, simpleProjectName.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("Email", QueryComparisons.Equal, email.ToLower()));

            var result = table.ExecuteQuery(query).ToList();
            if (result.Count > 0)
                pat = result[0].PAT;

            return pat;
        }

        public static string GetProjectName(string subdomain, string simpleProjectName)
        {
            string projectName = string.Empty;

            AzureTableHelper azureTableHelper = new AzureTableHelper();
            CloudTable table = azureTableHelper.GetAccountTable();

            TableQuery<Account> query = new TableQuery<Account>().Where(TableQuery.GenerateFilterCondition("SubdomainName", QueryComparisons.Equal, subdomain.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("SimpleProjectName", QueryComparisons.Equal, simpleProjectName.ToLower()));

            var result = table.ExecuteQuery(query).ToList();
            if (result.Count > 0)
                projectName = result[0].ProjectName;

            return projectName;
        }

        public static bool CheckIfAccountExists(string projectName, string subdomainName, string email)
        {
            AzureTableHelper azureTableHelper = new AzureTableHelper();
            CloudTable table = azureTableHelper.GetAccountTable();

            TableQuery<Account> query = new TableQuery<Account>()
                .Where(TableQuery.GenerateFilterCondition("ProjectName", QueryComparisons.Equal, projectName.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("SubdomainName", QueryComparisons.Equal, subdomainName.ToLower()))
                .Where(TableQuery.GenerateFilterCondition("Email", QueryComparisons.Equal, email.ToLower()));

            var result = table.ExecuteQuery(query).ToList();
            if (result.Count > 0)
                return true;
            else
                return false;
        }
    }
}