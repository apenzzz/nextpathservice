﻿using Microsoft.CSharp.RuntimeBinder;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Security.Application;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NextPathService.Helper
{
    public static class HttpClientExtensions
    {
        public static Task<HttpResponseMessage> PatchAsync(this HttpClient client, string requestUri, HttpContent content)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = new HttpMethod("PATCH"),
                RequestUri = new Uri(client.BaseAddress + requestUri),
                Content = content,
            };

            return client.SendAsync(request);
        }
    }

    public class VSTSHelper
    {
        private static HttpClient client = new HttpClient();

        public static async Task<bool> PostComment(string vstsSubdomain, string projectName, string pat, string workItemId, string comment)
        {
            string encodedProjectName = Uri.EscapeUriString(projectName);

            string url = "https://" + vstsSubdomain + ".visualstudio.com/" + encodedProjectName + 
                "/_apis/wit/workitems/" + workItemId + "?api-version=4.1";

            string escapedComment = comment.Replace("\"", "\\\"");

            var raw =
                " [  { \"op\": \"add\", \"path\": \"/fields/System.History\", \"value\": \"" + escapedComment + "\" } ]";

            var stringContent = new StringContent(raw, UnicodeEncoding.UTF8, "application/json-patch+json");
            client.DefaultRequestHeaders.Authorization = 
                new AuthenticationHeaderValue("Basic", EncodingHelper.Base64Encode(":" + pat));
            client.DefaultRequestHeaders
                  .Accept.Add(new MediaTypeWithQualityHeaderValue("application/json-patch+json"));
            var response = HttpClientExtensions.PatchAsync(
                client, url, stringContent).Result;
            var responseString = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
                return true;
            else
            {
                throw new Exception("VSTS Comment Failed : " + responseString);
            }
        }

        public static async Task<string> PostAttachment(string vstsSubdomain, string projectName, string pat, Stream stream, string fileName)
        {
            string encodedProjectName = Uri.EscapeUriString(projectName);

            string url = "https://dev.azure.com/" + vstsSubdomain + "/" + encodedProjectName +
                "/_apis/wit/attachments/?fileName=" + fileName + "&api-version=4.1";

            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Basic", EncodingHelper.Base64Encode(":" + pat));
            client.DefaultRequestHeaders
                  .Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));

            var inputData = new StreamContent(stream);
            var response = await client.PostAsync(url, inputData);

            var responseString = await response.Content.ReadAsStringAsync();
            dynamic responseObject = JsonConvert.DeserializeObject(responseString);

            string result = string.Empty;

            try
            {
                result = responseObject.url;
            }
            catch (RuntimeBinderException)
            {
                
            }

            return result;
        }

        public static bool ValidateRequest(string issuedToken)
        {
            string secret = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Im9PdmN6NU1fN3AtSGpJS2xGWHo5M3VfVjBabyJ9.eyJjaWQiOiJjMTA4NGNjYS1mOGEzLTQxNzQtODllNS05OTdiZTUwYTFjMjQiLCJjc2kiOiJkZjNhNzc0ZS1hNDQ2LTQzZGYtODhjNC1iN2Q1OWMxOTExNTYiLCJuYW1laWQiOiIwMDAwMDAyOS0wMDAwLTg4ODgtODAwMC0wMDAwMDAwMDAwMDAiLCJpc3MiOiJhcHAudnNzcHMudmlzdWFsc3R1ZGlvLmNvbSIsImF1ZCI6ImFwcC52c3Nwcy52aXN1YWxzdHVkaW8uY29tIiwibmJmIjoxNTMyODQ5NTExLCJleHAiOjE2OTA2MTU5MTF9.f6yf-5Au4lY5mbToY5XRLw7EyA4q8MlWdCU6JqmyfUyDKWhURD68vbQSQj4vMofX11rKsnBwalTVeZOzNkL3EWa7Agn6jpCNnYLtl0BjcjiI3R9SuzngxFpI4Hzu70x-uaunhkl_jZiOGt9HsqQhaDgXoQCWxaW7aQ9vgPHeqIrviOWlL_WfX0zjh8uvuLIIh5FfoAJy6WWIcBhP-0Ak7Z07_Atb3sQ7Koms_Aq_v5pK4YlBwigI-cOFcPovU4odoa-dwu8pOcBMWl6-x0INv37o4zWWq_Rich_fqJL1MLJd1iwt5qJNTcC96-44lxSdYPINjEecy6qAcuMVv9RqWA";

            var validationParameters = new TokenValidationParameters()
            {
                IssuerSigningKey = new SymmetricSecurityKey(System.Text.UTF8Encoding.UTF8.GetBytes(secret)),
                ValidateIssuer = false,
                RequireSignedTokens = true,
                RequireExpirationTime = true,
                ValidateLifetime = true,
                ValidateAudience = false,
                ValidateActor = false
            };

            SecurityToken token = null;
            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(issuedToken, validationParameters, out token);

            return principal.Identity.IsAuthenticated;
        }

        // organization is improvement for projectname naming parameter
        public static async Task<int> GetTotalAssignedLicenses(string organization, string pat)
        {
            try
            {
                int result = 0;

                string url = "https://vsaex.dev.azure.com/" + organization + "/_apis/userentitlementsummary?api-version=4.1-preview.1";

                client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Basic", EncodingHelper.Base64Encode(":" + pat));

                var response = await client.GetAsync(url);
                var responseString = await response.Content.ReadAsStringAsync();
                dynamic responseObject = JsonConvert.DeserializeObject(responseString);

                foreach (dynamic eachLicense in responseObject.licenses)
                {
                    result += eachLicense.assigned.Value;
                }

                return result;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}