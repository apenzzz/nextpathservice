﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace NextPathService.Helper
{
    public class EmailReplyParser
    {
        /*
         HTML Reply will clear node with this specs : 
         - div with class gmail_quote
         */
        public static string Reply(string html)
        {
            var htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//div");
            foreach (var node in htmlNodes)
            {
                if (node.Attributes.Count > 0)
                {
                    foreach (var attr in node.Attributes)
                    {
                        if (attr.Name == "class" && attr.Value == "gmail_quote")
                        {
                            node.Remove();
                        }
                    }
                }
            }

            string result = htmlDoc.DocumentNode.WriteTo();
            return result;
        }

        public static string RReply(string text)
        {
            EmailReplyMessage emailMessage = new EmailReplyMessage(text);
            emailMessage.Read();
            return emailMessage.Reply();
        }
    }

    public class EmailReplyMessage
    {
        string SIG_REGEX = @"(--|__|-\w)|(^Sent from my (\w+\s*){1,3})";
        string QUOTE_HDR_REGEX = @"On.*wrote:$";
        string QUOTED_REGEX = @"(>+)";
        string HEADER_REGEX = @"^(From|Sent|To|Subject): .+";
        string _MULTI_QUOTE_HDR_REGEX = @"(?!On.*On\s.+?wrote:)(On\s(.+?)wrote:)";
        string MULTI_QUOTE_HDR_REGEX = @"(?!On.*On\s.+?wrote:)(On\s(.+?)wrote:)"; // re.DOTALL | re.MULTILINE
        string MULTI_QUOTE_HDR_REGEX_MULTILINE = @"(?!On.*On\s.+?wrote:)(On\s(.+?)wrote:)"; // re.DOTALL

        List<Fragment> fragments = new List<Fragment>();
        Fragment fragment = new Fragment();
        string text;
        bool found_visible = false;

        public EmailReplyMessage(string Text)
        {
            text = Text.Replace("\r\n", "\n");
        }

        public void Read()
        {
            found_visible = false;

            string[] lines = text.Split('\n');
            var reversedLines = lines.Reverse();

            foreach (var line in reversedLines)
            {
                ScanLine(line);
            }

            FinishFragment();

            fragments.Reverse();
        }

        public void ScanLine(string line)
        {
            bool isQuoteHeader = Regex.IsMatch(line, QUOTE_HDR_REGEX);
            bool isQuote = Regex.IsMatch(line, QUOTED_REGEX);
            bool isHeader = Regex.IsMatch(line, HEADER_REGEX);

            if (fragment.lines.Count > 1 && string.IsNullOrEmpty(line.Trim()))
            {
                string lastLine = fragment.lines[fragment.lines.Count - 1];
                bool isSignature = Regex.IsMatch(lastLine, SIG_REGEX);
                if (isSignature)
                {
                    fragment.signature = true;
                    FinishFragment();
                }
            }

            if ((fragment.headers == isHeader && fragment.quoted == isQuote) || fragment.quoted && (isQuote || string.IsNullOrEmpty(line.Trim())))
            {
                fragment.lines.Add(line);
            }
            else
            {
                FinishFragment();
                fragment = new Fragment(isQuote, line, isHeader);
            }
        }

        public void FinishFragment()
        {
            if (fragment.lines.Count > 0)
            {
                fragment.Finish();

                if (fragment.headers)
                {
                    found_visible = false;
                    foreach (var each in fragments)
                    {
                        each.hidden = true;
                    }
                }

                if (!found_visible)
                {
                    if (fragment.quoted || fragment.headers || fragment.signature || string.IsNullOrEmpty(fragment.Content()))
                    {
                        fragment.hidden = true;
                    }
                    else
                    {
                        found_visible = true;
                    }
                }

                fragments.Add(fragment);
            }

            fragment = new Fragment();
        }

        public string Reply()
        {
            List<string> result = new List<string>();
            foreach (var item in fragments)
            {
                if (!(item.hidden || item.quoted))
                {
                    result.Add(item.Content());
                }
            }

            return string.Join("\n", result);
        }
    }

    public class Fragment
    {
        public bool signature = false;
        public bool headers;
        public bool hidden = false;
        public bool quoted;
        public string _content;
        public List<string> lines = new List<string>();

        public Fragment()
        {

        }

        public Fragment(bool isquoted, string firstline, bool isheaders)
        {
            headers = isheaders;
            quoted = isquoted;
            _content = string.Empty;
            lines.Add(firstline);
        }

        public void Finish()
        {
            lines.Reverse();
            _content = string.Join("\n", lines);
            lines = new List<string>();
        }

        public string Content()
        {
            return _content;
        }
    }
}