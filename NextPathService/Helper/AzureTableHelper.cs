﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using NextPathService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NextPathService.Helper
{
    public class AzureTableHelper
    {
        public CloudTable GetAccountTable()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(Constant.ACCOUNT_TABLENAME);
            table.CreateIfNotExists();

            return table;
        }

        public CloudTable GetEmailLogTable()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(Constant.EMAILLOG_TABLENAME);
            table.CreateIfNotExists();

            return table;
        }

        public CloudTable GetTaskLogTable()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(Constant.TASKLOG_TABLENAME);
            table.CreateIfNotExists();

            return table;
        }

        public void UpsertAccount(Account account)
        {
            CloudTable accountTable = GetAccountTable();
            TableOperation insertOperation = TableOperation.InsertOrReplace(account);
            accountTable.Execute(insertOperation);
        }

        public void InsertEmailLog(EmailLog emailLog)
        {
            CloudTable table = GetEmailLogTable();
            TableOperation insertOperation = TableOperation.Insert(emailLog);
            table.Execute(insertOperation);
        }
    }
}